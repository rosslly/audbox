【ems截图报错解决】
要保证工具所在文件夹路径中没有中文，否则ems截图会报错，无法使用！！！

审计工具箱 v5.1 更新内容
【大】修复天眼查工商截图在火狐浏览器下无法使用的问题
【大】失信被执行人黑名单功能更换接口，原有的两个失信人查询功能合并为一个全新功能，同时现在有两种查询模式：一是模糊查询，仅在B列填人名或者公司名称；二是精确查询，在B列填人名或者公司名称，并且在C列填写身份证或组织机构代码
【大】优化询证函斜线替换功能，替换的成功率大幅提高
【小】部分报错提示的优化
